import csv
from datetime import datetime
from io import StringIO

from django import forms
from django.db import models
from django.db.models import Sum
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .models import Orders


class CSVDocument(models.Model):
    file = models.FileField()


class CSVFileForm(forms.ModelForm):
    class Meta:
        model = CSVDocument
        fields = ('file', )


def index(request):
    return render(
        request,
        'index.html'
    )


def result(request):
    sort_column = request.GET.get('sort_by', 'shipping_address_zip')
    sort_direction = request.GET.get('sort', 'asc')
    data = Orders.objects.values(
        'shipping_address_zip'
    ).annotate(
        total_price_with_shipping_sum=Sum('total_price_with_shipping')
    )
    if sort_column:
        data = data.order_by('{}{}'.format('-' if sort_direction == 'desc' else '', sort_column))

    return render(
        request,
        'result.html',
        {'data': data, 'sort': sort_direction}
    )


def upload(request):
    if request.method == 'POST':
        form = CSVFileForm(request.POST, request.FILES)
        if form.is_valid():
            csv_data = csv.DictReader(StringIO(request.FILES['file'].read().decode('utf-8', 'backslashreplace')), delimiter=';')
            for row in csv_data:
                row['created_at'] = datetime.strptime(row['created_at'], '%d.%m.%Y %H:%M')
                row['processed_at'] = datetime.strptime(row['processed_at'], '%d.%m.%Y %H:%M')
                for float_row_name in ['total_price_with_shipping', 'total_price_without_shipping', 'total_shipping', 'total_item_price', 'item_price', 'former_price', 'price_range_amount', 'quantity', 'refund_amounts']:
                    row[float_row_name] = row[float_row_name] if row[float_row_name] else 0
                try:
                    Orders.objects.create(**row)
                except Exception as err:
                    print(row)
                    print(err)
                    continue
        return HttpResponseRedirect('/result')
    else:
        form = CSVFileForm()

    return render(
        request,
        'upload.html',
        {'form': form}
    )
